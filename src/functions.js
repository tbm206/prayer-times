export const snd = ([_, a]) => a;
export const fst = ([a, _]) => a;

const trigonometricDeg = f => deg => f((deg * Math.PI) / 180);

export const cos = trigonometricDeg(Math.cos);
export const sin = trigonometricDeg(Math.sin);
export const tan = trigonometricDeg(Math.tan);
export const arcsin = trigonometricDeg(Math.asin);
export const arctan = trigonometricDeg(Math.atan);
export const arctan2 = (x, y) => Math.atan2(x, y) * 180 / Math.PI;

const fix = b => a => {
  const c = a - b * Math.floor(a / b);

  return c < 0 ? c + b : c;
};

export const fixAngle = fix(360);
export const fixHour = fix(24);
