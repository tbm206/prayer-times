import {
  interval,
  combineLatest,
  forkJoin,
} from 'rxjs';
import {
  bufferCount,
  filter,
  map,
  startWith,
} from 'rxjs/operators';
import { run } from '@cycle/rxjs-run';
import { makeDOMDriver } from '@cycle/dom';
import Snabbdom from 'snabbdom-pragma';
import {
  add,
  getHours,
  getMinutes,
  getSeconds,
} from 'date-fns';
import { format } from "date-fns/fp";

import { dailyPrayerTimes$, prayerNotifications$ } from "./streams";

const TIME_FORMAT = 'HH:mm';
const CLOCK_FORMAT = 'HH:mm:ss';
const formatTime = format(TIME_FORMAT);
const formatClock = format(CLOCK_FORMAT);

const date = new Date();

const timeNow$ = interval(1000).pipe(
  map(t => add(date, { seconds: t }))
);

const drivers = {
  DOM: makeDOMDriver("#root"),
};
function main(_sources) {
  const state$ = combineLatest(
    timeNow$,
    dailyPrayerTimes$,
    prayerNotifications$
  );
  const sinks = {
    DOM: state$.pipe(
      map(([timeNow, prayerTimes, currentPrayer]) =>
        <div className="prayer-times">
          <h3>{formatClock(timeNow)}</h3>
          <table>
            <thead>
              <tr>
                <th>Prayer</th>
                <th>Time</th>
              </tr>
            </thead>
            <tbody>
              {prayerTimes.map(([prayerName, { start }]) =>
                <tr className={currentPrayer === prayerName ? "current-prayer" : ""}>
                  <td>{prayerName}</td>
                  <td>{formatTime(start)}</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      )
    )
  };
  return sinks;
}

run(main, drivers);
