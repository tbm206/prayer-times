import {
  add,
  getYear,
  getMonth,
  getDate,
} from "date-fns/fp";
import startOfToday from "date-fns/startOfToday";
import startOfTomorrow from "date-fns/startOfTomorrow";
import { applyTo, compose, map, zip } from "ramda";

import {
  FAJIR,
  DHUHUR,
  ASIR,
  MAGHRIB,
  ISHA,
} from "./constants";
import { fixAngle, fixHour, cos, sin, arctan2 } from "./functions";

const gregorianToJulian = (date = new Date()) => {
  const year = getYear(date);
  const month = getMonth(date);
  const day = getDate(date);

  return (1461 * (year + 4800 + (month - 14) / 12)) / 4
    + (367 * (month - 2 - 12 * ((month - 14) / 12))) / 12
    - (3 * ((year + 4900 + (month - 14) / 12) / 100)) / 4
    + day
    - 32075;
};

const getEquationOfTime = (julianDay) => {
  const n = julianDay - 2451545.0;
  const g = fixAngle(357.528 + 0.9856003 * n);
  const q = fixAngle(280.459 + 0.98564736 * n);
  const lambda = fixAngle(q + 1.915 * sin(g) + 0.020 * sin(2 * g));

  const R = 1.00014 - 0.01671 * cos(g) - 0.00014 * cos(2 * g);
  const e = 23.439 - 0.00000036 * n;
  const RA = arctan2(cos(e) * sin(lambda), cos(lambda)) / 15;

  return q / 15 - fixHour(RA);
};

const adjustForDaylightSaving = (hour, isDaylightSavingTime) =>
  isDaylightSavingTime ? hour + 1 : hour;

export default (latitude = 50.90623870747429, longitude = -1.395858385615321, isDaylightSavingTime = true) => {
  const today = startOfToday();
  const now = new Date();
  const timezone = adjustForDaylightSaving(now.getTimezoneOffset() / 60, isDaylightSavingTime);
  const prayers = [FAJIR, DHUHUR, ASIR, MAGHRIB, ISHA];

  const middleLongitude = timezone * 15;
  const longitudeDifference = (middleLongitude - longitude) / 15;
  const equationOfTime = getEquationOfTime(gregorianToJulian(now));

  const dhuhur = adjustForDaylightSaving(12 + longitudeDifference - equationOfTime, isDaylightSavingTime);

  const prayerTimes =
    map(applyTo(today))(
      [ add({ hours: 5 })
      , add({ hours: 12 })
      , add({ hours: 15 })
      , add({ hours: 18 })
      , add({ hours: 20, minutes: 37, seconds: 30 })
      ]
    );

  const tomorrow = startOfTomorrow();

  const customZip = ([first, ...rest]) => zip([first, ...rest], [...rest, tomorrow]);
  const toInterval = ([start, end]) => ({ start, end });

  return compose(zip(prayers), map(toInterval), customZip)(prayerTimes);
}
