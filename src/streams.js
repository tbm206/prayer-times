import {
  getHours,
  getMinutes,
  getSeconds,
  isWithinInterval,
} from "date-fns";
import { interval, merge, of } from "rxjs";
import {
  map,
  withLatestFrom,
  startWith,
  filter,
} from "rxjs/operators";
import {
  compose,
  curry,
  filter as filterR,
  map as mapR
} from "ramda";

import computePrayerTimes from "./computePrayerTimes";
import { fst, snd } from "./functions";

const laterPrayerTimes$ = interval(1000).pipe(
  map(() => new Date()),
  filter(t => [getHours, getMinutes, getSeconds].every(f => f(t) === 0)),
  map(computePrayerTimes),
);

export const dailyPrayerTimes$ = merge(of(computePrayerTimes()), laterPrayerTimes$);

const flippedIsWithinInterval = curry(isWithinInterval);
const filterCurrentPrayer = t => compose(fst, fst, filterR(compose(flippedIsWithinInterval(t), snd)));

export const prayerNotifications$ = interval(2000).pipe(
  withLatestFrom(dailyPrayerTimes$),
  map(([_, prayers]) => filterCurrentPrayer(new Date())(prayers))
);
